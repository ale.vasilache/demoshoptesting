package org.fasttrackit.EndToEndTest;

import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EndToEndUserDinoTest extends TestConfiguration {

    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnResetButton();
        homePage.clickOnTheLogoButton();
    }

    @Test
    public void user_dino_end_to_end_experience_adding_product1_to_cart() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField("dino");
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed");
        Header header = new Header("dino");
        assertEquals(header.getGreetingsMessage(), "Hi dino!", "Greetings message to contain the account user: Hi Dino!");
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        assertEquals(cartPage.getCartPageName(), "Your cart", "Expected Page Name to be: Your cart");
        assertEquals(cartPage.getProductPieces(), "1", "Expected Product pieces to be 1");
        assertEquals(cartPage.getProductPricePerPrice(), "$15.99", "Expected Product Price per piece to be: $15.99");
        assertEquals(cartPage.getProductPriceTotal(), "$15.99", "Expected Product price total to be: $15.99");
        assertEquals(cartPage.getItemTotalPrice(), "$15.99", "Message Items Total Price to be: $15.99");
        assertEquals(cartPage.getTaxPrice(), "$0.80", "Expected tax price to be: $0.80");
        assertEquals(cartPage.getTotalAmountPrice(), "$15.99", "Expected tax price to be: $15.99");
        assertTrue(cartPage.validateContinueShoppingButton(), "Expected Continue shopping button to exists on the page");
        assertTrue(cartPage.validateCheckOutButton(), "Expected Checkout button to exists on the page");
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        assertEquals(checkoutPage.getCheckOutPageTitle(), "Your information", "Expected page title to be: Your information");
        checkoutPage.typeInFirstNameField("Robert");
        checkoutPage.typeInLastNameField("Manolache");
        checkoutPage.typeInAddressField("Jucu Herghelie 2");
        assertTrue(checkoutPage.validateCancelCheckOut(), "Expected cancel button to be displayed");
        assertTrue(checkoutPage.validateContinueCheckOut(), "Expected Continue checkout to be displayed");
        checkoutPage.clickOnContinueCheckOut();
        OrderSummary orderSummary = new OrderSummary();
        assertEquals(orderSummary.getOrderPageSummaryTitle(), "Order summary", "Page <Order summary> to be displayed");
        assertTrue(orderSummary.validateCancelButton(), "Expected Cancel button to exists");
        assertTrue(orderSummary.validateCompleteYourOrder(), "Expected complete your order button to be displayed");
        orderSummary.clickOnCompleteYourOrder();
        OrderCompletePage orderCompletePage = new OrderCompletePage();
        assertTrue(orderCompletePage.validateCompletePageTitle(), "Page title expected to be displayed");
        assertEquals(orderCompletePage.getOrderPageTitle(), "Order complete", "Expected page title to be: Order complete");
        assertEquals(orderCompletePage.getDefaultMessage(), "Thank you for your order!", "Expected default message to be: Thank you for your order");
        orderCompletePage.clickOnContinueShopping();
        assertEquals(homePage.getPageTitle(), "Demo shop", "Expected page title to be: Products");
    }


}
