package org.fasttrackit.Scenarios;

import io.qameta.allure.Feature;
import org.fasttrackit.Products.Product;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductDetailsPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class ProductHomePageTest extends TestConfiguration {
    MainPage homePage = new MainPage();

    @AfterMethod
    public void returnToHomePage() {

        homePage.clickOnTheLogoButton();
    }

    @Test(testName = "Verify if product page can be open when clicking on the product test",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void verify_product_page_can_open_test(Product p) {
        p.clickOnProduct();
        ProductDetailsPage productPage = new ProductDetailsPage();
        String expectedTitle = p.getExpectedResults().getTitle();
        assertEquals(productPage.getTitleInfo(), p.getExpectedResults().getTitle(), "Expected title to be " + expectedTitle);
    }

    @Test(testName = "Verify if product price from home page is the same as the one from product page",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void verify_price_on_product_page_same_as_one_from_home_page(Product p) {
        p.clickOnProduct();
        ProductDetailsPage productPage = new ProductDetailsPage();
        String expectedPrice = p.getExpectedResults().getPrice();
        assertEquals(productPage.getPriceInfo(), p.getExpectedResults().getPrice(), "Expected price to be " + expectedPrice);
    }

    @Test(testName = "Verify if product can be added to basket without user logged",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void verify_default_user_can_add_product_to_cart_test(Product p) {
        p.addProductToBasket();
        String expectedCardNumber = "1";
        assertEquals(homePage.getProductAddedToCardNumber(), expectedCardNumber, "Expected number added to card is " + expectedCardNumber);
        homePage.clickOnCartButton();
        String expectedTitle = p.getExpectedResults().getTitle();
        assertEquals(p.getExpectedResults().getTitle(), expectedTitle, "Expected Title to be " + expectedTitle);
        homePage.clickOnResetButton();
    }
}
