package org.fasttrackit.Scenarios;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.Account;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.CheckoutPage;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.OrderSummary;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CheckoutPageTest extends TestConfiguration {

    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {

        homePage.clickOnResetButton();
        homePage.clickOnTheLogoButton();
    }

    @Test(testName = "Verify Checkout feature with correct credential")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_checkout_with_correct_credentials() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.typeInFirstNameField("Robert");
        checkoutPage.typeInLastNameField("Manolache");
        checkoutPage.typeInAddressField("Jucu Herghelie nr 2");
        checkoutPage.clickOnContinueCheckOut();
        OrderSummary orderSummary = new OrderSummary();
        assertEquals(orderSummary.getOrderPageSummaryTitle(), "Order summary", "Page <Order summary> to be displayed");
    }

    @Test(testName = "Verify Checkout possibility without First Name")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cannot_checkout_without_firstname() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.typeInLastNameField("Manolache");
        checkoutPage.typeInAddressField("Jucu Herghelie nr 2");
        checkoutPage.clickOnContinueCheckOut();
        assertTrue(checkoutPage.validateErrorField(), "Expected Error Message is displayed");
        assertEquals(checkoutPage.getErrorMessage(), "First Name is required", "Expected Error Message to be: First Name is require ");
    }

    @Test(testName = "Verify Checkout possibility without Last Name")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cannot_checkout_without_lastname() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.typeInFirstNameField("Robert");
        checkoutPage.typeInAddressField("Jucu Herghelie nr 2");
        checkoutPage.clickOnContinueCheckOut();
        assertTrue(checkoutPage.validateErrorField(), "Expected Error Message is displayed");
        assertEquals(checkoutPage.getErrorMessage(), "Last Name is required", "Expected Error Message to be:Last Name is required");
    }

    @Test(testName = "Verify Checkout possibility without address")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cannot_checkout_without_address() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.typeInFirstNameField("Robert");
        checkoutPage.typeInLastNameField("Manolache");
        checkoutPage.clickOnContinueCheckOut();
        assertTrue(checkoutPage.validateErrorField(), "Expected Error Message is displayed");
        assertEquals(checkoutPage.getErrorMessage(), "Address is required", "Expected Error Message to be:Address is required");
    }

    @Test(testName = "Verify Checkout possibility without credential")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cannot_checkout_without_credential() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.clickOnContinueCheckOut();
        assertTrue(checkoutPage.validateErrorField(), "Expected Error Message is displayed");
        assertEquals(checkoutPage.getErrorMessage(), "First Name is required", "Expected Error Message to be: First Name is require ");
    }

    @Test(testName = "Verify Cancel button inside checkout page")
    public void user_can_cancel_checkout() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.clickOnCancelButton();
        assertTrue(cartPage.validateCartPageIsDisplayed(), "Expected Cart Page to be displayed");
        assertEquals(cartPage.getCartPageName(), "Your cart", "Expected Cart Page Name to be: Your cart");
    }


}
