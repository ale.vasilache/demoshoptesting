package org.fasttrackit.Scenarios;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.CheckoutPage;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.OrderCompletePage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class OrderSummaryAndCompletionTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @BeforeTest
    public void setup() {
        homePage.clickOnResetButton();
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnResetButton();
        homePage.returnToHomePage();
    }

    @Test(testName = "Verify user can complete order")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_finalize_checkout_in_order_summary_page() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.typeInFirstNameField("Robert");
        checkoutPage.typeInLastNameField("Manolache");
        checkoutPage.typeInAddressField("Jucu Herghelie nr 2");
        checkoutPage.clickOnContinueCheckOut();
        org.fasttrackit.pages.OrderSummary orderSummary = new org.fasttrackit.pages.OrderSummary();
        assertEquals(orderSummary.getOrderPageSummaryTitle(), "Order summary", "Page <Order summary> to be displayed");
        orderSummary.clickOnCompleteYourOrder();
        OrderCompletePage orderCompletePage = new OrderCompletePage();
        assertTrue(orderCompletePage.validateCompletePageTitle(), "Page title expected to be displayed");
        assertEquals(orderCompletePage.getOrderPageTitle(), "Order complete", "Expected page title to be: Order complete");
    }

    @Test(testName = "Verify user can cancel order from order summary page ")
    public void user_can_cancel_checkout_in_order_summary_page() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.typeInFirstNameField("Robert");
        checkoutPage.typeInLastNameField("Manolache");
        checkoutPage.typeInAddressField("Jucu Herghelie nr 2");
        checkoutPage.clickOnContinueCheckOut();
        org.fasttrackit.pages.OrderSummary orderSummary = new org.fasttrackit.pages.OrderSummary();
        assertEquals(orderSummary.getOrderPageSummaryTitle(), "Order summary", "Page <Order summary> to be displayed");
        orderSummary.clickOnCancelButton();
        assertTrue(cartPage.validateCartPageIsDisplayed(), "Page title expected to be displayed");
        assertEquals(cartPage.getCartPageName(), "Your cart", "Page title to be: Your cart");
    }

    @Test(testName = "Verify user can continue shopping after order is completed")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_continue_shopping_after_checkout_in_order_complete_page() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.typeInFirstNameField("Robert");
        checkoutPage.typeInLastNameField("Manolache");
        checkoutPage.typeInAddressField("Jucu Herghelie nr 2");
        checkoutPage.clickOnContinueCheckOut();
        org.fasttrackit.pages.OrderSummary orderSummary = new org.fasttrackit.pages.OrderSummary();
        orderSummary.clickOnCompleteYourOrder();
        OrderCompletePage orderCompletePage = new OrderCompletePage();
        orderCompletePage.clickOnContinueShopping();
        assertEquals(homePage.getPageTitle(), "Demo shop", "Expected page title to be: Demo shop");
    }
}