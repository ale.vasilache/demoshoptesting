package org.fasttrackit.Scenarios;

import io.qameta.allure.Feature;
import org.fasttrackit.Products.Product;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class WishListHomePageTest extends TestConfiguration {
    MainPage homePage = new MainPage();

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
    }

    @Test(testName = "Verify if product can be added to wishlist without user logged from Home Page",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void user_can_add_product_to_wishlist_test(Product p) {
        p.addProductToWishlist();
        String expectedWishlistNumber = "1";
        assertEquals(homePage.getProductAddedToWishListNumber(), expectedWishlistNumber, "Expected number added to card is " + expectedWishlistNumber);
        homePage.clickOnTheWishlistButton();
        String expectedTitle = p.getExpectedResults().getTitle();
        assertEquals(p.getExpectedResults().getTitle(), expectedTitle, "Expected Title to be " + expectedTitle);
        homePage.clickOnResetButton();
    }

    @Test(testName = "Verify if product can be removed from wishlist without user logged from Home Page",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void verify_product_can_be_removed_from_wishlist(Product p) {
        p.addProductToWishlist();
        String expectedWishlistNumber = "1";
        assertEquals(homePage.getProductAddedToWishListNumber(), expectedWishlistNumber, "Expected number added to card is " + expectedWishlistNumber);
        p.removeProductFromWishlist();
        assertTrue(homePage.validateWishlistNumberIsNotDisplayed(), "Expected Wishlist number is not displayed on the page");
        homePage.clickOnResetButton();
    }

}
