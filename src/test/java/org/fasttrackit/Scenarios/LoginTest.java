package org.fasttrackit.Scenarios;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.Account;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class LoginTest extends TestConfiguration {

    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.clickOnResetButton();
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnResetButton();
        homePage.returnToHomePage();
    }

    @Test(testName = "Login Test with valid user credential ",
            dataProvider = "AccountDataProvider",
            dataProviderClass = AccountDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_login_on_the_demo_app_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed");
        Header header = new Header(account.getUsername());
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Greetings message to contain the account user. ");
        homePage.clickOnLogUserOut();
    }

    @Test(testName = "Verify user cannot login with locked account")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cannot_login_with_locked_account_test() {
        Account lockedAccount = new Account("locked", "choochoo");

        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(lockedAccount.getUsername());
        modal.typeInPasswordField(lockedAccount.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain open");
        modal.clickOnCloseButton();
    }

    @Test(testName = "Verify user cannot login on page without password")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cannot_login_on_page_without_password_test() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "beetle";
        modal.typeInUsernameField(user);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertTrue(modal.validateErrorMarkIsDisplayed(), "Expected error mark `Exclamation point` is displayed");
        assertEquals(modal.getLoginErrorText(), "Please fill in the password!");
        modal.clickOnCloseButton();
        sleep(2000);
    }

    @Test(testName = "Verify user cannot login on page with wrong password")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cannot_login_on_page_with_wrong_password_test() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "beetle";
        modal.typeInUsernameField(user);
        modal.typeInWrongPasswords();
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertTrue(modal.validateErrorMarkIsDisplayed(), "Expected error mark `Exclamation point` is displayed");
        assertEquals(modal.getLoginErrorText(), "Incorrect username or password!");
        modal.clickOnCloseButton();
        sleep(2000);
    }

    @Test(testName = "Verify user cannot login on page without username")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cannot_login_on_page_without_username_test() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "";
        modal.typeInUsernameField(user);
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertTrue(modal.validateErrorMarkIsDisplayed(), "Expected error mark `Exclamation point` is displayed");
        assertEquals(modal.getLoginErrorText(), "Please fill in the username!");
        modal.clickOnCloseButton();
    }

    @Test(testName = "Verify user cannot login on page with wrong username")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cannot_login_on_page_with_wrong_username_test() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInWrongUsername();
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertTrue(modal.validateErrorMarkIsDisplayed(), "Expected error mark `Exclamation point` is displayed");
        assertEquals(modal.getLoginErrorText(), "Incorrect username or password!");
        modal.clickOnCloseButton();
    }
}

