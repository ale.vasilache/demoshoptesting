package org.fasttrackit.Scenarios;

import io.qameta.allure.Feature;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.CheckoutPage;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static org.testng.Assert.assertFalse;


public class CartPageTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @BeforeTest
    public void setup() {
        homePage.clickOnResetButton();
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnResetButton();
        homePage.returnToHomePage();
    }

    @Test(testName = "Verify cart page items when product quantity is increasing")
    public void verify_increasing_the_product_quantity_in_cart_page() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnPlusItem();

        assertEquals(cartPage.getProductPieces(), "2", "Expected Product pieces to be 2");
        assertEquals(cartPage.getProductPricePerPrice(), "$15.99", "Expected Product Price per piece to be: $15.99");
        assertEquals(cartPage.getProductPriceTotal(), "$31.98", "Expected Product price total to be: $31.98");
        assertEquals(cartPage.getItemTotalPrice(), "$31.98", "Message Items Total Price to be: $31.98");
        assertEquals(cartPage.getTaxPrice(), "$1.60", "Expected tax price to be: $1.60");
        assertEquals(cartPage.getTotalAmountPrice(), "$31.98", "Expected tax price to be: $31.98");
    }

    @Test(testName = "Verify cart page items when product quantity is decreasing to 1 from 2")
    public void verify_decreasing_the_product_quantity_in_cart_page_to_minimum_1() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnPlusItem();
        assertEquals(cartPage.getProductPieces(), "2", "Expected Product pieces to be 2");
        assertEquals(cartPage.getProductPricePerPrice(), "$15.99", "Expected Product Price per piece to be: $15.99");
        assertEquals(cartPage.getProductPriceTotal(), "$31.98", "Expected Product price total to be: $31.98");
        assertEquals(cartPage.getItemTotalPrice(), "$31.98", "Message Items Total Price to be: $31.98");
        assertEquals(cartPage.getTaxPrice(), "$1.60", "Expected tax price to be: $1.60");
        assertEquals(cartPage.getTotalAmountPrice(), "$31.98", "Expected tax price to be: $31.98");

        cartPage.clickOnMinusItem();
        assertEquals(cartPage.getProductPieces(), "1", "Expected Product pieces to be 1");
        assertEquals(cartPage.getProductPricePerPrice(), "$15.99", "Expected Product Price per piece to be: $15.99");
        assertEquals(cartPage.getProductPriceTotal(), "$15.99", "Expected Product price total to be: $15.99");
        assertEquals(cartPage.getItemTotalPrice(), "$15.99", "Message Items Total Price to be: $15.99");
        assertEquals(cartPage.getTaxPrice(), "$0.80", "Expected tax price to be: $0.80");
        assertEquals(cartPage.getTotalAmountPrice(), "$15.99", "Expected tax price to be: $15.99");
    }

    @Test(testName = "Verify cart page items when product quantity is decreasing to 1 from 0")
    public void verify_decreasing_the_product_quantity_in_cart_page_to_0() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        assertEquals(cartPage.getProductPieces(), "1", "Expected Product pieces to be 1");
        assertEquals(cartPage.getProductPricePerPrice(), "$15.99", "Expected Product Price per piece to be: $15.99");
        assertEquals(cartPage.getProductPriceTotal(), "$15.99", "Expected Product price total to be: $15.99");
        assertEquals(cartPage.getItemTotalPrice(), "$15.99", "Message Items Total Price to be: $15.99");
        assertEquals(cartPage.getTaxPrice(), "$0.80", "Expected tax price to be: $0.80");
        assertEquals(cartPage.getTotalAmountPrice(), "$15.99", "Expected tax price to be: $15.99");
        cartPage.clickOnMinusItem();

        String expectedResultIntroText = "How about adding some products in your cart?";
        assertEquals(cartPage.getIntroText(), expectedResultIntroText, "Expected title to be: " + expectedResultIntroText);

        assertFalse(cartPage.validateMinusItemIsDisplayed(), "Expected minus (-) item  NOT to be displayed");
        assertFalse(cartPage.validateProductPiecesIsDisplayed(), "Expected pieces of the selected product NOT to be displayed");
        assertFalse(cartPage.validatePlusItemIsDisplayed(), "Expected plus (+) item NOT to be displayed");
        assertFalse(cartPage.validatePricePerPieceIsDisplayed(), "Expected product pieces NOT to be displayed");
        assertFalse(cartPage.validateProductPriceTotalIsDisplayed(), "Expected price total NOT to be displayed");
        assertFalse(cartPage.validateItemNameIsDisplayed(), "Expected product name NOT to be displayed");
        assertFalse(cartPage.validateDeleteIcon(), "Expected trash icon NOT to be displayed");
        assertFalse(cartPage.validateItemTotalText(), "Expected item total text NOT to be displayed");
        assertFalse(cartPage.validateItemTotalPrice(), "Expected item total price NOT to be displayed");
        assertFalse(cartPage.validateTaxText(), "Expected Tax text NOT to be displayed");
        assertFalse(cartPage.validateTaxPrice(), "Expected tax price NOT to be displayed");
        assertFalse(cartPage.validateTotalAmountText(), "Expected total amount text NOT to be displayed");
        assertFalse(cartPage.validateTotalAmountPrice(), "Expected total amount price NOT to ve displayed");
        assertFalse(cartPage.validateContinueShoppingButton(), "Expected Continue Shopping Button NOT to be displayed");
        assertFalse(cartPage.validateCheckOutButton(), "Expected Checkout button NOT to be displayed");
    }

    @Test(testName = "Verify if deleting the product from cart page will bring user back to Main Page")
    public void verify_trash_button_from_cart_page_when_one_product_on_cart() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnDeleteItem();
        assertEquals(homePage.getPageTitle(), "Demo shop", "Application title is expected to be: Demo shop");

    }

    @Test(testName = "Verify if Continue Shopping button from cart page goes back to Main Page")
    public void verify_continue_shopping_button_from_cart_page() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnContinueShopping();
        assertEquals(homePage.getPageTitle(), "Demo shop", "Application title is expected to be: Demo shop");
    }


    @Test(testName = "Verify if Checkout button goes to checkout page")
    public void verify_continue_checkout_button_from_cart_page() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        assertEquals(checkoutPage.getCheckOutPageTitle(), "Your information", "Expected page title to be: ");
    }

}
