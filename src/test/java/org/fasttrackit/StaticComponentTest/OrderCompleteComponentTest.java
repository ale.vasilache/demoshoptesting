package org.fasttrackit.StaticComponentTest;

import io.qameta.allure.Feature;
import org.fasttrackit.pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class OrderCompleteComponentTest {


    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnResetButton();
    }

    @Test(testName = "Verify component at Order Completion Page")
    public void verify_static_component_in_order_completion_page() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.typeInFirstNameField("Vasile");
        checkoutPage.typeInLastNameField("Manolescu");
        checkoutPage.typeInAddressField("Ramnicu Valcea, Strada principala");
        checkoutPage.clickOnContinueCheckOut();
        OrderSummary orderSummaryPage = new OrderSummary();
        orderSummaryPage.clickOnCompleteYourOrder();
        OrderCompletePage orderCompletePage = new OrderCompletePage();
        assertTrue(orderCompletePage.validateCompletePageTitle(), "Expected Order complete page title to be displayed");
        assertEquals(orderCompletePage.getOrderPageTitle(), "Order complete", "Expected page title to be: Order complete");
        assertTrue(orderCompletePage.validateDefaultMessage(), "Default message to be displayed");
        assertEquals(orderCompletePage.getDefaultMessage(), "Thank you for your order!", "Expected default message to be: Thank you for your order!");
        assertTrue(orderCompletePage.validateContinueShoppingButton(), "Expected Continue shopping button to be displayed on the screen");
    }

}
