package org.fasttrackit.StaticComponentTest;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Feature;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class StaticApplicationStateTest extends TestConfiguration {
    MainPage homePage = new MainPage();

    /**
     * Demo Shop Application Tittle
     */
    @Test
    public void verify_demo_shop_app_title_test() {
        String appTitle = homePage.getPageTitle();
        assertEquals(appTitle, "Demo shop", "Application title is expected to be: Demo shop");
    }

    /**
     * Demo Shop Footer Static Verification
     */
    @Test
    public void verify_demoShop_footer_build_date_details_test() {
        String footerDetails = homePage.getFooter().getDetails();
        assertEquals(footerDetails, "Demo Shop | build date 2021-05-21 14:04:30 GTBDT", "Expected footer details to be: Demo Shop | build date 2021-05-21 14:04:30 GTBDT");
    }

    @Test
    public void verify_demoShop_footer_contains_question_icon_test() {
        SelenideElement questionIcon = homePage.getFooter().getQuestionIcon();
        assertTrue(questionIcon.exists(), "Expected question icon does not exists on page");
        assertTrue(questionIcon.isDisplayed(), "Expected question icon is not displayed on page");
    }

    @Test
    public void verify_demoShop_footer_contains_reset_icon_test() {
        SelenideElement resetIconTitle = homePage.getFooter().getResetIcon();
        assertTrue(resetIconTitle.exists(), "Expected reset icon does not exists on page");
        assertTrue(resetIconTitle.isDisplayed(), "Expected reset icon is not displayed on page");
    }

    /**
     * Demo Shop Header Static Verification
     */

    @Test
    public void verify_demo_shop_header_contains_logo_icon_test() {
        SelenideElement logoIcon = homePage.getHeader().getLogoIconUrl();
        assertTrue(logoIcon.exists(), "Expected Logo Icon to exist on page");
        assertTrue(logoIcon.isDisplayed(), "Expected Logo Icon to be displayed");
    }

    @Test
    public void verify_demo_shop_header_contains_cart_icon_test() {
        SelenideElement cartIcon = homePage.getHeader().getShoppingCartIconUrl();
        assertTrue(cartIcon.exists(), "Expected Cart Icon to exist on Page");
        assertTrue(cartIcon.isDisplayed(), "Excepted Cart icon to be displayed");
    }

    @Test
    public void verify_demo_shop_header_contains_greetings_message() {
        String greetingsMessage = String.valueOf(homePage.getHeader().getGreetingsMessage());
        assertEquals(greetingsMessage, "Hello guest!", "Expected greeting message to be: Hello quest!");
    }

    @Test
    public void verify_Demo_Shop_Header_Contains_Login_Button() {
        SelenideElement loginButton = homePage.getHeader().getLoginButton();
        assertTrue(loginButton.exists(), "Expected Login Button to exist on Page");
        assertTrue(loginButton.isDisplayed(), "Expected Login Button to be displayed");
    }


    @Test
    public void verify_demo_shop_header_contains_wishlist_icon_test() {
        SelenideElement wishlistIcon = homePage.getHeader().getWishlistIconUrl();
        assertTrue(wishlistIcon.exists(), "Expected Wishlist Icon to exist on page");
        assertTrue(wishlistIcon.isDisplayed(), "Expected Wishlist Icon to be displayed");
    }

}
