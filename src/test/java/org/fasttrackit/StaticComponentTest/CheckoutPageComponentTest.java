package org.fasttrackit.StaticComponentTest;

import io.qameta.allure.Feature;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.CheckoutPage;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class CheckoutPageComponentTest extends TestConfiguration {

    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnResetButton();
    }

    @Test(testName = "Verify component at Checkout Page in Default Mode")
    public void verify_static_component_checkout_page_in_default_mode() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        assertTrue(checkoutPage.validateCheckoutPageTitle(), "Checkout Page title to be displayed");
        assertEquals(checkoutPage.getCheckOutPageTitle(), "Your information", "Expected checkout page title to be: Your information");
        assertTrue(checkoutPage.validateAddressInformationHeader(), "Address Information Header to be displayed ");
        assertEquals(checkoutPage.getAddressInformationHeader(), "Address information", "Expected Header information to be: Address Information");
        assertTrue(checkoutPage.validateFirstNameField(), "Expected First Name Field to be displayed");
        assertTrue(checkoutPage.validateLastNameField(), "Expected Last Name Field to be displayed");
        assertTrue(checkoutPage.validateAddressField(), "Expected Address Field to be displayed");
        assertTrue(checkoutPage.validateDeliveryInformationHeader(), "Expected delivery information header to be displayed");
        assertEquals(checkoutPage.getDeliveryInformationHeader(), "Delivery information", "Expected delivery header information to be: Delivery information");
        assertTrue(checkoutPage.validateChooChooDeliveryInformation(), "Expected Choo Choo delivery to be displayed");
        assertEquals(checkoutPage.getChoochooDeliveryInformation(), "Choo Choo delivery", "Expected result to be: Choo Choo delivery");
        assertTrue(checkoutPage.validatePaymentInformationHeader(), "Expected Payment information header to be visible");
        assertEquals(checkoutPage.getPaymentInformationHeader(), "Payment information", "Expected payment information header to be: Payment information");
        assertTrue(checkoutPage.validateCashOnDelivery(), "Expected Cash on Delivery to be displayed");
        assertEquals(checkoutPage.getCashOnDelivery(), "Cash on delivery", "Expected delivery option to be: Cash on delivery");
        assertTrue(checkoutPage.validateCreditCard(), "Expected Credit Card to be displayed");
        assertEquals(checkoutPage.getCreditCard(), "Credit card", "Expected delivery option to be: Credit Card");
        assertTrue(checkoutPage.validatePayPal(), "Expected PayPal to be displayed");
        assertEquals(checkoutPage.getPayPal(), "PayPal", "Expected option to be: PayPal");
        assertTrue(checkoutPage.validateCancelCheckOut(), "Expected Cancel button to be displayed");
        assertTrue(checkoutPage.validateContinueCheckOut(), "Expected Continue checkout button to be displayed");
        assertFalse(checkoutPage.validateErrorField(), "Expected Error Field NOT to be displayed");
    }

}
