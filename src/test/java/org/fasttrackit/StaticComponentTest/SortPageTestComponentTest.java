package org.fasttrackit.StaticComponentTest;

import io.qameta.allure.Feature;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.SortDropDownMenu;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;


public class SortPageTestComponentTest extends TestConfiguration {
    MainPage mainPage = new MainPage();

    @AfterMethod
    public void returnToHomePage() {
        mainPage.clickOnTheLogoButton();
        mainPage.clickOnResetButton();
    }

    @Test
    public void verify_sort_field_is_displayed_test() {
        Assert.assertTrue(mainPage.validateSortFieldIsDisplayed(), "Expected sort field to be displayed");
    }

    @Test
    public void verify_default_AtoZ_sort_page_component_test() {
        SortDropDownMenu sortPage = new SortDropDownMenu();
        Assert.assertTrue(sortPage.validateSortAtoZIsDisplayedByDefault(), "Expected Sort by Name (AtoZ) to be displayed");
    }

    @Test
    public void verify_Selected_ZtoA_sort_page_component_test() {
        SortDropDownMenu sortPage = new SortDropDownMenu();
        mainPage.clickOnSortMenu();
        Assert.assertTrue(sortPage.validateSortZtoAIsDisplayed(), "Expected Sort by Name (ZtoA) to be displayed");
    }

    @Test
    public void verify_Selected_LowToHigh_sort_page_component_test() {
        SortDropDownMenu sortPage = new SortDropDownMenu();
        mainPage.clickOnSortMenu();
        Assert.assertTrue(sortPage.validateSortByPriceLowToHighIsDisplayed(), "Expected Sort by Price Low to High to be displayed");
    }

    @Test
    public void verify_Selected_HighToLow_sort_page_component_test() {
        SortDropDownMenu sortPage = new SortDropDownMenu();
        mainPage.clickOnSortMenu();
        Assert.assertTrue(sortPage.validateSortByPriceHighToLowIsDisplayed(), "Expected Sort by Price High to Low to be displayed");
    }
}
