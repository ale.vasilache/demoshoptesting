package org.fasttrackit.StaticComponentTest;

import io.qameta.allure.Feature;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class CartPageComponentTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @BeforeTest
    public void setup() {
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnResetButton();
    }

    @Test(testName = "Verify if intro text is displayed and is correct")
    public void verify_cart_page_static_component_in_default_mode() {
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        assertTrue(cartPage.validateCartPageIsDisplayed(), "Expected cart page title to be displayed");
        String expectedResultTitle = "Your cart";
        assertEquals(cartPage.getCartPageName(), expectedResultTitle, "Expected title to be: " + expectedResultTitle);
        assertTrue(cartPage.validateIntroTextIsDisplayed(), "Expected intro text to be displayed on the page ");
        String expectedResultIntroText = "How about adding some products in your cart?";
        assertEquals(cartPage.getIntroText(), expectedResultIntroText, "Expected title to be: " + expectedResultIntroText);

        assertFalse(cartPage.validateMinusItemIsDisplayed(), "Expected minus (-) item  NOT to be displayed");
        assertFalse(cartPage.validateProductPiecesIsDisplayed(), "Expected pieces of the selected product NOT to be displayed");
        assertFalse(cartPage.validatePlusItemIsDisplayed(), "Expected plus (+) item NOT to be displayed");
        assertFalse(cartPage.validatePricePerPieceIsDisplayed(), "Expected product pieces NOT to be displayed");
        assertFalse(cartPage.validateProductPriceTotalIsDisplayed(), "Expected price total NOT to be displayed");
        assertFalse(cartPage.validateItemNameIsDisplayed(), "Expected product name NOT to be displayed");
        assertFalse(cartPage.validateDeleteIcon(), "Expected trash icon NOT to be displayed");
        assertFalse(cartPage.validateItemTotalText(), "Expected item total text NOT to be displayed");
        assertFalse(cartPage.validateItemTotalPrice(), "Expected item total price NOT to be displayed");
        assertFalse(cartPage.validateTaxText(), "Expected Tax text NOT to be displayed");
        assertFalse(cartPage.validateTaxPrice(), "Expected tax price NOT to be displayed");
        assertFalse(cartPage.validateTotalAmountText(), "Expected total amount text NOT to be displayed");
        assertFalse(cartPage.validateTotalAmountPrice(), "Expected total amount price NOT to ve displayed");
        assertFalse(cartPage.validateContinueShoppingButton(), "Expected Continue Shopping Button NOT to be displayed");
        assertFalse(cartPage.validateCheckOutButton(), "Expected Checkout button NOT to be displayed");
        ;
    }


    @Test(testName = "Verify static components on Cart page for one product added on the page")
    public void verify_cart_page_when_one_product_is_added() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();

        assertTrue(cartPage.validateCartPageIsDisplayed(), "Expected cart page title to be displayed");
        String expectedResultTitle = "Your cart";
        assertEquals(cartPage.getCartPageName(), expectedResultTitle, "Expected title to be: " + expectedResultTitle);

        assertFalse(cartPage.validateIntroTextIsDisplayed(), "Expected intro text NOT to be displayed on the page ");

        assertTrue(cartPage.validateMinusItemIsDisplayed(), "Expected minus (-) item to be displayed");

        assertTrue(cartPage.validateProductPiecesIsDisplayed(), "Expected pieces of the selected product to be displayed");
        assertEquals(cartPage.getProductPieces(), "1", "Expected product quantity to be 1");

        assertTrue(cartPage.validatePlusItemIsDisplayed(), "Expected plus (+) item to be displayed");

        assertTrue(cartPage.validatePricePerPieceIsDisplayed(), "Expected price per pieces to be displayed");
        assertEquals(cartPage.getProductPricePerPrice(), "$15.99", "Expected price to be $15.99");

        assertTrue(cartPage.validateProductPriceTotalIsDisplayed(), "Expected price total to be displayed");
        assertEquals(cartPage.getProductPriceTotal(), "$15.99", "Expected price to be $15.99");

        assertTrue(cartPage.validateItemNameIsDisplayed(), "Expected product name to be displayed");
        assertEquals(cartPage.getItemName(), "Awesome Granite Chips", "Expected product name to be: Awesome Granite Chips ");

        assertTrue(cartPage.validateDeleteIcon(), "Expected trash icon to be displayed");

        assertTrue(cartPage.validateItemTotalText(), "Expected item total text to be displayed");
        assertEquals(cartPage.getItemTotalText(), "Items total:", "Expected items total text to be: Items total");

        assertTrue(cartPage.validateItemTotalPrice(), "Expected item total price to be displayed");
        assertEquals(cartPage.getItemTotalPrice(), "$15.99", "Expected items total price to be: $15.99");

        assertTrue(cartPage.validateTaxText(), "Expected Tax text to be displayed");
        assertEquals(cartPage.getTaxText(), "Tax:", "Expected Tax text to be: Tax:");

        assertTrue(cartPage.validateTaxPrice(), "Expected Tax price to be displayed");
        assertEquals(cartPage.getTaxPrice(), "$0.80", "Expected tax price to be $0.8");

        assertTrue(cartPage.validateTotalAmountText(), "Expected total amount text to be displayed");
        assertEquals(cartPage.getTotalAmountText(), "Total:", "Expected total amount text to be: Total:");

        assertTrue(cartPage.validateTotalAmountPrice(), "Expected total amount price to be displayed");
        assertEquals(cartPage.getTotalAmountPrice(), "$15.99", "Expected total amount to be: $15.99");

        assertTrue(cartPage.validateContinueShoppingButton(), "Expected Continue Shopping Button to be displayed");
        assertEquals(cartPage.getContinueShopping(), "Continue Shopping", "Expected Continue shopping button name to be: Continue shopping");

        assertTrue(cartPage.validateCheckOutButton(), "Expected Checkout button to be displayed");
        assertEquals(cartPage.getCheckout(), "Checkout", "Expected Checkout button name to be: > Checkout");
    }
}
