package org.fasttrackit.StaticComponentTest;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Feature;
import org.fasttrackit.Products.Product;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductDetailsPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;


public class ProductDetailPageComponentTest extends TestConfiguration {

    MainPage homePage = new MainPage();

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
    }

    @Test(testName = "Verify static elements on product pages",
            dataProvider = "productsDataProvider",
            dataProviderClass = ProductDataProvider.class)
    public void verify_product_page_detail_page_test(Product p) {
        p.clickOnProduct();
        ProductDetailsPage productPage = new ProductDetailsPage();

        SelenideElement productTitle = productPage.getTitleDisplay();
        SelenideElement productPhoto = productPage.getPhoto();
        SelenideElement productPrice = productPage.getPriceDisplay();
        SelenideElement productBasket = productPage.getBasket();
        SelenideElement productWishlistAdd = productPage.getWishlistAdd();
        SelenideElement productWishlistRemove = productPage.getWishlistRemove();
        SelenideElement productStock = productPage.getStockDisplay();

        assertTrue(productTitle.exists(), "Expected Product Title to exist on Page)");
        assertTrue(productTitle.isDisplayed(), "Expected Product Title to be displayed");

        assertTrue(productPhoto.exists(), "Expected Product Photo to exist on Page)");
        assertTrue(productPhoto.isDisplayed(), "Expected Product Photo to be displayed");

        assertTrue(productPrice.exists(), "Expected Product Price to exist on Page");
        assertTrue(productPrice.isDisplayed(), "Expected Product Price to be displayed");

        assertTrue(productBasket.exists(), "Expected Add to Basket Button to exist on Page)");
        assertTrue(productBasket.isDisplayed(), "Expected Add to Basket Button to be displayed");

        assertTrue(productWishlistAdd.exists(), "Expected Add to Wishlist Button to exist on Page");
        assertTrue(productWishlistAdd.isDisplayed(), "Expected Wishlist button to be displayed");

        assertFalse(productWishlistRemove.exists(), "Expected Remove Wishlist Button to exist on Page");
        assertFalse(productWishlistRemove.isDisplayed(), "Expected Remove Wishlist Button not displayed in default Mode");

        assertTrue(productStock.exists(), "Expected Product Stock Information to exist on Page");
        assertTrue(productStock.isDisplayed(), "Expected Product Stock Information to be displayed");

    }
}
