package org.fasttrackit.StaticComponentTest;

import io.qameta.allure.Feature;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class LoginModalComponentTest extends TestConfiguration {

    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToHomePage();
    }

    @Test(testName = "Verify modal components are displayed on the page,",
            description = "This test is responsible with testing if modal components are visible and modal can be closed "
    )
    public void verify_Login_modal_components_are_displayed_and_modal_can_be_closed_test() {
        homePage.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to be displayed");
        Modal modal = new Modal();
        assertEquals(modal.getModalTitle(), "Login", "Expected login title to be Login");
        assertTrue(modal.validateCloseButtonIsDisplayed(), "Expected close button to be displayed");
        assertTrue(modal.validateUsernameFieldIsDisplayed(), "Expected username field to be displayed");
        assertTrue(modal.validatePasswordFieldIsDisplayed(), "Expected password field to be displayed");
        assertTrue(modal.validateLoginButtonIsDisplayed(), "Expected login button is displayed");
        assertTrue(modal.validateLoginButtonIsEnabled(), "Expected login button is enabled");
        modal.clickOnCloseButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed");
    }

}
