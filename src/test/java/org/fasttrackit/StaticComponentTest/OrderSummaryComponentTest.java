package org.fasttrackit.StaticComponentTest;

import io.qameta.allure.Feature;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.CheckoutPage;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.OrderSummary;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class OrderSummaryComponentTest extends TestConfiguration {
    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToHomePage();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnResetButton();
    }

    @Test(testName = "Verify component at Order Summary in Default Mode")
    public void verify_static_component_checkout_page_product1_in_cart() {
        homePage.clickOnAddToCartProduct1();
        homePage.clickOnCartButton();
        CartPage cartPage = new CartPage();
        cartPage.clickOnCheckOut();
        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.typeInFirstNameField("Vasile");
        checkoutPage.typeInLastNameField("Manolescu");
        checkoutPage.typeInAddressField("Ramnicu Valcea, Strada principala");
        checkoutPage.clickOnContinueCheckOut();
        OrderSummary orderSummaryPage = new OrderSummary();
        assertTrue(orderSummaryPage.validateOrderPageTitle(), "Expected Order summary page title to be displayed");
        assertEquals(orderSummaryPage.getOrderPageSummaryTitle(), "Order summary", "Expected Order summary page title to be: Order summary");
        assertTrue(orderSummaryPage.validatePaymentInformation(), "Expected Payment information to be displayed");
        assertEquals(orderSummaryPage.getPaymentInformation(), "Payment Information:", "Expected payment information header title to be: Payment Information");
        assertTrue(orderSummaryPage.validateCashOnDelivery(), "Expected Cash on delivery to be displayed");
        assertEquals(orderSummaryPage.getPaymentMethod(), "Cash on delivery", "Expected Payment Option to be: Cash on delivery");
        assertTrue(orderSummaryPage.validateShipmentInformation(), "Expected Shipment information to be displayed");
        assertEquals(orderSummaryPage.getShippingInformation(), "Shipping Information:", "Expected shipping information to be: Shipping Information: ");
        assertTrue(orderSummaryPage.validateChoochooDelivery(), "Expected delivery to be displayed on the page");
        assertEquals(orderSummaryPage.getChoochooDelivery(), "CHOO CHOO DELIVERY!", "Expected delivery to be: CHOO CHOO DELIVERY!");
        assertTrue(orderSummaryPage.validateItemsTotalText(), "Expected items total text to be displayed on the page");
        assertEquals(orderSummaryPage.getItemsTotalText(), "Items total:", "Expected items total text to be: Items total:");
        assertTrue(orderSummaryPage.validateItemsTotalPrice(), "Expected items price to be displayed on the page");
        assertEquals(orderSummaryPage.getItemsTotalPrice(), "$15.99", "Expected items total price to be: $15.99");
        assertTrue(orderSummaryPage.validateTaxText(), "Expected tax text to be displayed on page");
        assertEquals(orderSummaryPage.getTaxText(), "Tax:", "Expected  tax title to be: tax");
        assertTrue(orderSummaryPage.validateTaxPrice(), "Expected tax price  to be displayed");
        assertEquals(orderSummaryPage.getTaxPrice(), "$0.80", "Expected tax price to be: $0.80");
        assertTrue(orderSummaryPage.validateAmountTotalText(), "Expected total amount to be displayed");
        assertEquals(orderSummaryPage.getTotalAmountText(), "Total:", "Expected total amount text to be: Total:");
        assertTrue(orderSummaryPage.validateAmountTotalPrice(), "Expected total to be displayed");
        assertEquals(orderSummaryPage.getTotalAmountPrice(), "$15.99", "Expected total amount sum to be: $15.99");
        assertTrue(orderSummaryPage.validateCancelButton(), "Expected Cancel button to be displayed");
        assertTrue(orderSummaryPage.validateCompleteYourOrder(), "Expected complete your order button to be displayed");

    }

}



