package org.fasttrackit.body;

import com.codeborne.selenide.ClickOptions;
import com.codeborne.selenide.SelenideElement;

import java.sql.SQLOutput;
import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Modal {
    private final SelenideElement modalTitle = $(".modal-title");
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".btn-primary");
    private final SelenideElement loginErrorText = $(".error");
    private final SelenideElement loginErrorMark = $("[data-icon=exclamation-circle]");

    /**
     * Getters
     */
    public String getModalTitle() {
        return modalTitle.text();
    }

    public String getLoginErrorText() {
        return loginErrorText.text();
    }

    public SelenideElement getLoginErrorMark() {
        return loginErrorMark;
    }

    /**
     * Click Actions
     */
    public void clickOnCloseButton() {
        System.out.println("Clicked on the 'x' button ");
        this.closeButton.click();
        sleep(150);
    }

    public void clickUsernameField() {
        System.out.println("Clicked on the username field ");
        this.username.click();
    }

    public void clickOnPasswordField() {
        System.out.println("Clicked on the password field ");
        this.password.click();
    }

    public void clickOnTheLoginButton() {
        System.out.println("Clicked o the Modal Login Button");
        this.loginButton.click();
        sleep(150);
    }

    /**
     * Validators of Static Elements
     */

    public boolean validateCloseButtonIsDisplayed() {
        return this.closeButton.exists() && this.closeButton.isDisplayed();
    }

    public boolean validateUsernameFieldIsDisplayed() {
        return this.username.exists() && this.username.isDisplayed();
    }

    public boolean validatePasswordFieldIsDisplayed() {
        return this.password.exists() && this.password.isDisplayed();
    }

    public boolean validateLoginButtonIsDisplayed() {
        return this.loginButton.exists() && this.loginButton.isDisplayed();
    }

    public boolean validateLoginButtonIsEnabled() {
        return this.loginButton.isEnabled();
    }

    /**
     * Type Actions
     */

    public void typeInUsernameField(String userToType) {
        System.out.println("Typed in username: " + userToType);
        this.username.click();
        this.username.sendKeys(userToType);
    }

    public void typeInWrongUsername() {
        System.out.println("Typed in wrong username");
        this.username.click();
        this.username.sendKeys("asdfghj");
    }

    public void typeInPasswordField(String passwordToType) {
        System.out.println("Typed in password: " + passwordToType);
        this.password.click();
        this.password.sendKeys(passwordToType);
    }

    public void typeInWrongPasswords() {
        System.out.println("Typed in wrong password");
        this.password.click();
        this.password.sendKeys("asdfghjkl");
    }

    public void validateModalComponents() {
    }

    public boolean validateErrorMarkIsDisplayed() {
        return this.loginErrorMark.exists() && this.loginErrorMark.isDisplayed();
    }
}
