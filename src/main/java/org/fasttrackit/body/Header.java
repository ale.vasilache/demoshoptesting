package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private final SelenideElement logoIconUrl = $(".fa-shopping-bag");
    private final SelenideElement shoppingCartIconUrl = $("[href='#/cart']");
    private final SelenideElement wishlistIconUrl = $("[href='#/wishlist']");
    private final SelenideElement greetingsMessage = $(".navbar-text span span");
    private final SelenideElement signInButton = $(".fa-sign-in-alt");
    private final SelenideElement signOutButton = $(".fa-sign-out-alt");
    private final SelenideElement pageWishlistSubtitle = $(".shopping-cart-icon .fa-heart~span");
    private final SelenideElement ProductAddedToCardNumber = $(".shopping-cart-icon .fa-shopping-cart~span");

    public Header() {
    }

    public Header(String user) {
    }

    /**
     * Getters
     */
    public SelenideElement getLogoIconUrl() {
        return this.logoIconUrl;
    }

    public SelenideElement getShoppingCartIconUrl() {
        return shoppingCartIconUrl;
    }

    public SelenideElement getWishlistIconUrl() {
        return wishlistIconUrl;
    }

    public String getGreetingsMessage() {
        return greetingsMessage.text();
    }

    public SelenideElement getLoginButton() {
        return signInButton;
    }

    /**
     * CLicks
     */
    public void clickOnTheLoginButton() {
        System.out.println("Clicked on the Sign-in button !");
        this.signInButton.click();
    }

    public void clickOnTheLogoutButton() {
        System.out.println("Clicked on the Sign-out button !");
        this.signOutButton.click();
    }

    public void clickOnTheWishlistButton() {

        System.out.println("Clicked on the Wishlist button !");
        this.wishlistIconUrl.click();
    }

    public void clickOnTheLogoButton() {
        System.out.println("Return to Home Page !");
        this.logoIconUrl.click();
    }

    public void clickOnCartButton() {
        System.out.println("Clicked on the Cart button !");
        this.shoppingCartIconUrl.click();
    }
}
