package org.fasttrackit.Products;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final SelenideElement productLink;
    private final SelenideElement card;
    private final SelenideElement addToBasket;
    private final SelenideElement addToWishlist;
    private final SelenideElement removeFromWishlist;
    private final SelenideElement expectedPrice;
    private final ProductExpectedResults expectedResults;

    public Product(String productID, ProductExpectedResults expectedResults) {
        this.productLink = $(String.format(".card-body [href='#/product/%s']", productID));
        this.expectedResults = expectedResults;
        this.card = productLink.parent().parent();
        this.addToBasket = card.$(".fa-cart-plus");
        this.removeFromWishlist = card.$(".fa-heart-broken");
        this.addToWishlist = card.$(".fa-heart");
        this.expectedPrice = card.$(".fa-span");
    }

    /**
     * Clicks Action
     */
    public void clickOnProduct() {
        this.productLink.scrollTo();
        this.productLink.click();
    }

    public void addProductToBasket() {
        this.addToBasket.scrollTo();
        this.addToBasket.click();
    }

    public void addProductToWishlist() {
        this.addToWishlist.scrollTo();
        this.addToWishlist.click();
    }

    public void removeProductFromWishlist() {
        this.removeFromWishlist.scrollTo();
        this.removeFromWishlist.click();
    }

    /**
     * Getters
     */
    public ProductExpectedResults getExpectedResults() {
        return expectedResults;
    }

    @Override
    public String toString() {
        return this.productLink.text();
    }

}
