package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class OrderCompletePage {

    private SelenideElement orderCompletePageTitle = $(".subheader-container");
    private SelenideElement defaultPageMesasge = $(".text-center");
    private SelenideElement continueShoppingButton = $(".btn-success");

    /**
     * Validators
     */

    public boolean validateCompletePageTitle() {
        System.out.println("Validate order complete page title is displayed");
        return this.orderCompletePageTitle.exists() && this.orderCompletePageTitle.isDisplayed();
    }

    public boolean validateDefaultMessage() {
        System.out.println("Validate default message is being displayed");
        return this.defaultPageMesasge.exists() && this.defaultPageMesasge.isDisplayed();
    }

    public boolean validateContinueShoppingButton() {
        System.out.println("validate Continue Shopping is displayed");
        return this.continueShoppingButton.exists() && this.continueShoppingButton.isDisplayed();
    }

    /**
     * Getters
     */

    public String getOrderPageTitle() {
        return this.orderCompletePageTitle.getText();
    }

    public String getDefaultMessage() {
        return this.defaultPageMesasge.getText();
    }

    /**
     * Clicks
     */


    public void clickOnContinueShopping() {
        this.continueShoppingButton.click();
    }

}
