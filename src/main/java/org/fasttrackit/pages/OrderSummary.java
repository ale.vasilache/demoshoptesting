package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class OrderSummary {

    private SelenideElement orderSummaryPageTitle = $(".subheader-container");
    private SelenideElement paymentInformation = $("div:nth-child(2) div.container:nth-child(2) div.col:nth-child(1) > div:nth-child(1)");
    private SelenideElement cashOnDelivery = $(" div.col:nth-child(1) > div:nth-child(2)");
    private SelenideElement shippingInformation = $(" div.col:nth-child(1) > div:nth-child(3)");
    private SelenideElement choochooDelivery = $(" div.col:nth-child(1) > div:nth-child(4)");
    private SelenideElement itemsTotalText = $("tr:nth-child(1) > td:nth-child(1)");
    private SelenideElement itemsTotalPrice = $("tr:nth-child(1) > td.amount:nth-child(2)");
    private SelenideElement taxText = $("tr:nth-child(2) > td:nth-child(1)");
    private SelenideElement taxPrice = $("tr:nth-child(2) > td.amount:nth-child(2)");
    private SelenideElement totalAmountText = $("tr.amount-total:nth-child(3) > td:nth-child(1)");
    private SelenideElement totalAmountPrice = $("tr.amount-total:nth-child(3) > td:nth-child(2)");
    private SelenideElement cancelButton = $(".btn-danger");
    private SelenideElement completeYourOrderButton = $(".btn-success");


    /**
     * Validators
     */

    public boolean validateOrderPageTitle() {
        System.out.println("Validate if Order summary page title is displayed");
        return this.orderSummaryPageTitle.exists() && this.orderSummaryPageTitle.isDisplayed();
    }

    public boolean validatePaymentInformation() {
        System.out.println("Validate if Payment information is displayed");
        return this.paymentInformation.exists() && this.paymentInformation.isDisplayed();
    }

    public boolean validateCashOnDelivery() {
        System.out.println("Validate if Cash on Delivery is displayed");
        return this.cashOnDelivery.exists() && this.cashOnDelivery.isDisplayed();
    }

    public boolean validateShipmentInformation() {
        System.out.println("Validate if Shipment information is displayed");
        return this.shippingInformation.exists() && this.shippingInformation.isDisplayed();
    }

    public boolean validateChoochooDelivery() {
        System.out.println("Validate if ChooChoo Delivery is displayed");
        return this.choochooDelivery.exists() && this.choochooDelivery.isDisplayed();
    }

    public boolean validateItemsTotalText() {
        System.out.println("Validate if Items total text is displayed");
        return this.itemsTotalText.exists() && this.itemsTotalText.isDisplayed();
    }

    public boolean validateItemsTotalPrice() {
        System.out.println("Validate if Items total price is displayed");
        return this.itemsTotalPrice.exists() && this.itemsTotalPrice.isDisplayed();
    }

    public boolean validateTaxText() {
        System.out.println("Validate if Tax total text is displayed");
        return this.taxText.exists() && this.taxText.isDisplayed();
    }

    public boolean validateTaxPrice() {
        System.out.println("Validate if Tax total price is displayed");
        return this.taxPrice.exists() && this.taxPrice.isDisplayed();
    }

    public boolean validateAmountTotalText() {
        System.out.println("Validate if Amount total text is displayed");
        return this.totalAmountText.exists() && this.totalAmountText.isDisplayed();
    }

    public boolean validateAmountTotalPrice() {
        System.out.println("Validate if Amount total price is displayed");
        return this.totalAmountPrice.exists() && this.totalAmountPrice.isDisplayed();
    }

    public boolean validateCancelButton() {
        System.out.println("Validate if cancellation button is displayed");
        return this.cancelButton.exists() && this.cancelButton.isDisplayed();
    }

    public boolean validateCompleteYourOrder() {
        System.out.println("Validate if complete you request button is displayed");
        return this.completeYourOrderButton.exists() && this.completeYourOrderButton.isDisplayed();
    }

    /**
     * Getters
     */

    public String getOrderPageSummaryTitle() {
        return orderSummaryPageTitle.getText();
    }

    public String getPaymentInformation() {
        return paymentInformation.getText();
    }

    public String getPaymentMethod() {
        return cashOnDelivery.getText();
    }

    public String getShippingInformation() {
        return shippingInformation.getText();
    }

    public String getChoochooDelivery() {
        return choochooDelivery.getText();
    }

    public String getItemsTotalText() {
        return itemsTotalText.getText();
    }

    public String getItemsTotalPrice() {
        return itemsTotalPrice.getText();
    }

    public String getTaxText() {
        return taxText.getText();
    }

    public String getTaxPrice() {
        return taxPrice.getText();
    }

    public String getTotalAmountText() {
        return totalAmountText.getText();
    }

    public String getTotalAmountPrice() {
        return totalAmountPrice.getText();
    }

    /**
     * Clicks
     */

    public void clickOnCancelButton() {
        this.cancelButton.click();
    }

    public void clickOnCompleteYourOrder() {
        this.completeYourOrderButton.click();
    }

}

