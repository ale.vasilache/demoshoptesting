package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutPage {

    private final SelenideElement checkoutPageTitle = $(".text-muted");
    private final SelenideElement addressInformation = $("div.col:nth-child(1) > div.section-title:nth-child(1)");
    private final SelenideElement firstNameField = $("#first-name");
    private final SelenideElement lastNameField = $("#last-name");
    private final SelenideElement addressField = $("#address");

    private final SelenideElement deliveryInformation = $("div.col:nth-child(2) > div.section-title:nth-child(1)");
    private final SelenideElement choochooDeliveryInformation = $("div:nth-child(1)> label.form-check-label");
    private final SelenideElement paymentInformation = $("div.col:nth-child(2) > div.section-title:nth-child(3)");
    private final SelenideElement cashOnDelivery = $("div:nth-child(4) div.form-check:nth-child(1)");
    private final SelenideElement creditCard = $("div:nth-child(4) div.form-check:nth-child(2)");
    private final SelenideElement payPal = $("div:nth-child(4) div.form-check:nth-child(3)");

    private final SelenideElement cancelCheckOut = $(".btn-danger");
    private final SelenideElement continueCheckOut = $(".btn-success");
    private final SelenideElement errorMessage = $(".error");


    /**
     * Validators
     */

    public boolean validateCheckoutPageTitle() {
        System.out.println("Validate if checkout page name exists and is displayed");
        return this.checkoutPageTitle.exists() && this.checkoutPageTitle.isDisplayed();
    }

    public boolean validateAddressInformationHeader() {
        System.out.println("Validate if address information header is displayed");
        return this.addressInformation.exists() && this.addressInformation.isDisplayed();
    }

    public boolean validateFirstNameField() {
        System.out.println("Validate if First name field exists and is displayed");
        return this.firstNameField.exists() && this.firstNameField.isDisplayed();
    }

    public boolean validateLastNameField() {
        System.out.println("Validate if Last name field exists and is displayed");
        return this.lastNameField.exists() && this.lastNameField.isDisplayed();
    }

    public boolean validateAddressField() {
        System.out.println("Validate if address field exists and is displayed");
        return this.addressField.exists() && this.addressField.isDisplayed();
    }

    public boolean validateDeliveryInformationHeader() {
        System.out.println("Validate if delivery information header exists and is displayed");
        return this.deliveryInformation.exists() && this.deliveryInformation.isDisplayed();
    }

    public boolean validateChooChooDeliveryInformation() {
        System.out.println("Validate if ChooChoo delivery information exists and is displayed");
        return this.choochooDeliveryInformation.exists() && this.choochooDeliveryInformation.isDisplayed();
    }

    public boolean validatePaymentInformationHeader() {
        System.out.println("Validate if payment information header exist and is displayed");
        return this.paymentInformation.exists() && this.paymentInformation.isDisplayed();
    }

    public boolean validateCashOnDelivery() {
        System.out.println("Validate selection option Cash on Delivery exists and is displayed");
        return this.cashOnDelivery.exists() && this.cashOnDelivery.isDisplayed();
    }

    public boolean validateCreditCard() {
        System.out.println("Validate selection option Credit Card exists and is displayed");
        return this.creditCard.exists() && this.creditCard.isDisplayed();
    }

    public boolean validatePayPal() {
        System.out.println("Validate selection option PayPal exists and is displayed");
        return this.payPal.exists() && this.payPal.isDisplayed();
    }

    public boolean validateCancelCheckOut() {
        System.out.println("Validate button Cancel check out exists and is displayed");
        return this.cancelCheckOut.exists() && this.cancelCheckOut.isDisplayed();
    }

    public boolean validateContinueCheckOut() {
        System.out.println("Validate button Continue check out exists and is displayed");
        return this.continueCheckOut.exists() && this.continueCheckOut.isDisplayed();
    }

    public boolean validateErrorField() {
        System.out.println("Validate if error field is displayed ");
        return this.errorMessage.exists() && this.errorMessage.isDisplayed();
    }

    /**
     * Getters
     */

    public String getCheckOutPageTitle() {
        return this.checkoutPageTitle.getText();
    }

    public String getAddressInformationHeader() {
        return this.addressInformation.getText();
    }

    public String getDeliveryInformationHeader() {
        return this.deliveryInformation.getText();
    }

    public String getChoochooDeliveryInformation() {
        return this.choochooDeliveryInformation.getText();
    }

    public String getPaymentInformationHeader() {
        return this.paymentInformation.getText();
    }

    public String getCashOnDelivery() {
        return this.cashOnDelivery.getText();
    }

    public String getCreditCard() {
        return this.creditCard.getText();
    }

    public String getPayPal() {
        return this.payPal.getText();
    }

    public String getCancelCheckOut() {
        return this.cancelCheckOut.getText();
    }

    public String getContinueCheckOut() {
        return this.continueCheckOut.getText();
    }

    public String getErrorMessage() {
        return this.errorMessage.getText();
    }

    /**
     * User to Type
     */

    public void typeInFirstNameField(String userToType) {
        System.out.println("Typed in firstname: " + userToType);
        this.firstNameField.click();
        this.firstNameField.sendKeys(userToType);
    }

    public void typeInLastNameField(String userToType) {
        System.out.println("Typed in lastname: " + userToType);
        this.lastNameField.click();
        this.lastNameField.sendKeys(userToType);
    }

    public void typeInAddressField(String userToType) {
        System.out.println("Typed in address: " + userToType);
        this.addressField.click();
        this.addressField.sendKeys(userToType);
    }

    /**
     * Click
     */

    public void clickOnCancelButton() {
        this.cancelCheckOut.click();
    }

    public void clickOnContinueCheckOut() {
        this.continueCheckOut.click();
    }

}
