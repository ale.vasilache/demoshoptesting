package org.fasttrackit.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class SortDropDownMenu {
    private final SelenideElement sortField = $(".sort-products-select");
    private final SelenideElement sortAtoZ = $("[value=az]");
    private final SelenideElement sortZtoA = $("[value=za]");
    private final SelenideElement sortLowToHigh = $("[value=lohi]");
    private final SelenideElement sortHighToLow = $("[value=hilo]");

    /**
     * Clicks
     */
    public void clickOnTheSortField() {
        this.sortField.click();
    }

    /**
     * Validators
     */

    public boolean validateSortAtoZIsDisplayedByDefault() {
        System.out.println("Verify that the Default Sort by name (A to Z) is displayed. ");
        this.sortAtoZ.shouldBe(Condition.appear);
        this.sortAtoZ.shouldBe(Condition.selected);
        return this.sortAtoZ.exists() && this.sortAtoZ.isDisplayed();
    }

    public boolean validateSortZtoAIsDisplayed() {
        System.out.println("Verify that Sort by name (Z to A) is displayed");
        return this.sortZtoA.exists() && this.sortZtoA.isDisplayed();
    }

    public boolean validateSortByPriceLowToHighIsDisplayed() {
        System.out.println("Verify that Sort by price (low to high) is displayed");
        return this.sortLowToHigh.exists() && this.sortLowToHigh.isDisplayed();
    }

    public boolean validateSortByPriceHighToLowIsDisplayed() {
        System.out.println("Verify that Sort by price (high to low) is displayed");
        return this.sortHighToLow.exists() && this.sortHighToLow.isDisplayed();
    }

}

