package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class MainPage extends Page {

    private final String title = Selenide.title();
    private Header header;
    private final Footer footer;
    private final SortDropDownMenu sortPage;
    private final SelenideElement modal = $(".modal-dialog");
    private final SelenideElement ProductAddedToWishlistNumber = $(".shopping-cart-icon .fa-heart~span");
    private final SelenideElement ProductAddedToCardNumber = $(".shopping-cart-icon .fa-shopping-cart~span");
    private final SelenideElement sortButton = $(".sort-products-select");
    private SelenideElement product1Link = $("div.col:nth-child(1) a.card-link");
    private SelenideElement product1Price = $("div.col:nth-child(1) span:nth-child(1)");
    private SelenideElement addToBasketProduct1 = $("div.col:nth-child(1) .fa-cart-plus");
    private SelenideElement addToWishlistProduct1 = $("div.col:nth-child(1) .fa-heart");
    private SelenideElement removeFromWishlistProduct2 = $("div.col:nth-child(2) .fa-heart-broken");
    private SelenideElement product2Link = $("div.col:nth-child(2) a.card-link");
    private SelenideElement product2Price = $("div.col:nth-child(2) span:nth-child(1)");
    private SelenideElement addToBasketProduct2 = $("div.col:nth-child(2) .fa-cart-plus");
    //    private SelenideElement addToWishlistProduct1 = $("div.col:nth-child(2) .fa-heart");
    private SelenideElement removeFromWishlistProduct1 = $("div.col:nth-child(1) .fa-heart-broken");


    public MainPage() {
        System.out.println("Constructing Header");
        this.header = new Header();
        System.out.println("Constructing Footer");
        this.footer = new Footer();
        this.sortPage = new SortDropDownMenu();
    }

    public void returnToHomePage() {
        this.header.getLogoIconUrl().click();
    }

    /**
     * Setters
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    public SelenideElement ProductAddedToWishlistNumber() {
        return ProductAddedToWishlistNumber;
    }

    /**
     * Getters
     */

    public Footer getFooter() {
        return footer;
    }

    public String getPageTitle() {
        return title;
    }

    public Header getHeader() {
        return header;
    }


    public String getProductAddedToCardNumber() {
        this.ProductAddedToCardNumber.scrollTo();
        return ProductAddedToCardNumber.getText();
    }

    public String getProductAddedToWishListNumber() {
        this.ProductAddedToWishlistNumber.scrollTo();
        return ProductAddedToWishlistNumber.getText();
    }

    public String getPriceProduct1() {
        return this.product1Price.getText();
    }


    /**
     * Validate
     */
    public boolean validateWishlistNumberIsNotDisplayed() {
        System.out.println("Verify that Wishlist Number is not displayed");
        return !ProductAddedToWishlistNumber().exists();
    }

    public boolean validateModalIsDisplayed() {
        System.out.println("Verify that the modal is Displayed");
        return this.modal.exists() && this.modal.isDisplayed();
    }

    public boolean validateModalIsNotDisplayed() {
        System.out.println("Verify that modal is not on page. Exists ");
        return !modal.exists();
    }

    public boolean validateSortFieldIsDisplayed() {
        System.out.println("Verify that the sort modal is displayed");
        return this.sortButton.exists() && this.sortButton.isDisplayed();
    }

    /**
     * Clicks
     */

    public void clickOnTheWishlistButton() {
        this.header.clickOnTheWishlistButton();
    }

    public void clickOnTheLogoButton() {
        this.header.clickOnTheLogoButton();
    }

    public void clickOnCartButton() {
        this.header.clickOnCartButton();
    }

    public void clickOnTheLoginButton() {
        this.header.clickOnTheLoginButton();
    }

    public void clickOnResetButton() {
        this.footer.getResetIcon().click();
    }

    public void clickOnLogUserOut() {
        this.header.clickOnTheLogoutButton();
        sleep(150);
    }

    public void clickOnSortMenu() {
        System.out.println("Clicked on the Sort Menu button !");
        this.sortButton.click();
    }

    public void clickOnAddToCartProduct1() {
        System.out.println("Clicked on add to cart Product 1!");
        this.addToBasketProduct1.click();
        ;
    }

    public void clickOnAddToWishListProduct1() {
        System.out.println("Clicked on add to wishlist Product 1!");
        this.addToWishlistProduct1.click();
    }

    public void removeFromWishlistProduct1() {
        System.out.println("Clicked on remove from wishlist Product1");
        this.removeFromWishlistProduct1.click();
    }
}


