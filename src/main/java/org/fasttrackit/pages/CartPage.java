package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {

    private SelenideElement cartPageName = $(".text-muted");
    private SelenideElement introText = $(".text-center.container");
    private SelenideElement minusItem = $("[data-icon=minus-circle]");
    private SelenideElement plusItem = $("[data-icon=plus-circle]");
    private SelenideElement productPieces = $("div.col-md-auto:nth-child(1) > div:nth-child(1)");
    private SelenideElement productPricePerPiece = $("div.col-md-auto:nth-child(2) > div:nth-child(1)");
    private SelenideElement productPriceTotal = $("div.col-md-auto:nth-child(3) > div:nth-child(1)");
    private SelenideElement itemName = $("#item_1_title_link");
    private SelenideElement deleteItem = $("[data-icon=trash]");
    private SelenideElement itemTotalText = $("tr:nth-child(1) > td:nth-child(1)");
    private SelenideElement itemTotalPrice = $("tr:nth-child(1) > td.amount:nth-child(2)");
    private SelenideElement taxText = $("tr:nth-child(2) > td:nth-child(1)");
    private SelenideElement taxPrice = $("tr:nth-child(2) > td.amount:nth-child(2)");
    private SelenideElement totalAmountText = $("tr.amount-total:nth-child(3) > td:nth-child(1)");
    private SelenideElement totalAmountPrice = $("tr.amount-total:nth-child(3) > td:nth-child(2)");
    private SelenideElement continueShopping = $(".btn-danger");
    private SelenideElement checkout = $(".btn-success");


    /**
     * Validators
     */

    public boolean validateCartPageIsDisplayed() {
        System.out.println("Validate if cart page name exists and is displayed");
        return this.cartPageName.exists() && this.cartPageName.isDisplayed();
    }

    public boolean validateIntroTextIsDisplayed() {
        System.out.println("Validate if intro text item exists and is displayed");
        return this.introText.exists() && this.introText.isDisplayed();
    }

    public boolean validateMinusItemIsDisplayed() {
        System.out.println("Validate if minus item exists and is displayed");
        return this.minusItem.exists() && this.minusItem.isDisplayed();
    }

    public boolean validatePlusItemIsDisplayed() {
        System.out.println("Validate if plus item exists and is displayed");
        return this.plusItem.exists() && this.plusItem.isDisplayed();
    }

    public boolean validateProductPiecesIsDisplayed() {
        System.out.println("Validate product pieces is displayed");
        return this.productPieces.exists() && this.productPieces.isDisplayed();
    }

    public boolean validatePricePerPieceIsDisplayed() {
        System.out.println("Validate Price per piece is displayed");
        return this.productPricePerPiece.exists() && this.productPricePerPiece.isDisplayed();
    }

    public boolean validateProductPriceTotalIsDisplayed() {
        System.out.println("Validate product price total is displayed");
        return this.productPriceTotal.exists() && this.productPriceTotal.isDisplayed();
    }

    public boolean validateItemNameIsDisplayed() {
        System.out.println("Validate item name is displayed");
        return this.itemName.exists() && this.itemName.isDisplayed();
    }

    public boolean validateDeleteIcon() {
        System.out.println("Validate icon to deleting a product is displayed");
        return this.deleteItem.exists() && this.deleteItem.isDisplayed();
    }

    public boolean validateItemTotalText() {
        System.out.println("Validate item total text is displayed");
        return this.itemTotalText.exists() && this.itemTotalText.isDisplayed();
    }

    public boolean validateItemTotalPrice() {
        System.out.println("Validate item total price is displayed");
        return this.itemTotalPrice.exists() && this.itemTotalPrice.isDisplayed();
    }

    public boolean validateTaxText() {
        System.out.println("Validate Tax Text is displayed");
        return this.taxText.exists() && this.taxText.isDisplayed();
    }

    public boolean validateTaxPrice() {
        System.out.println("Validate Tax Price is displayed");
        return this.taxPrice.exists() && this.taxPrice.isDisplayed();
    }

    public boolean validateTotalAmountText() {
        System.out.println("Validate Total Amount Text is displayed");
        return this.totalAmountText.exists() && this.totalAmountText.isDisplayed();
    }

    public boolean validateTotalAmountPrice() {
        System.out.println("Validate Total Amount Price is displayed");
        return this.totalAmountPrice.exists() && this.itemTotalPrice.isDisplayed();
    }

    public boolean validateContinueShoppingButton() {
        System.out.println("Validate Continue Shopping button exists");
        return this.continueShopping.exists() && this.continueShopping.isDisplayed();
    }

    public boolean validateCheckOutButton() {
        System.out.println("Validate Check out button exists");
        return this.checkout.exists() && this.checkout.isDisplayed();
    }

    /**
     * Getters
     */

    public String getCartPageName() {
        return this.cartPageName.getText();
    }

    public String getIntroText() {
        return this.introText.getText();
    }

    public String getProductPieces() {
        return this.productPieces.getText();
    }

    public String getProductPricePerPrice() {
        return this.productPricePerPiece.getText();
    }

    public String getProductPriceTotal() {
        return this.productPriceTotal.getText();
    }

    public String getItemName() {
        return this.itemName.getText();
    }

    public String getItemTotalText() {
        return this.itemTotalText.getText();
    }

    public String getItemTotalPrice() {
        return this.itemTotalPrice.getText();
    }

    public String getTaxText() {
        return this.taxText.getText();
    }

    public String getTaxPrice() {
        return this.taxPrice.getText();
    }


    public String getTotalAmountText() {
        return this.totalAmountText.getText();
    }

    public String getTotalAmountPrice() {
        return this.totalAmountPrice.getText();
    }

    public String getContinueShopping() {
        return this.continueShopping.getText();
    }

    public String getCheckout() {
        return this.checkout.getText();
    }

    /**
     * Clicks
     */

    public void clickOnPlusItem() {
        this.plusItem.click();
    }

    public void clickOnMinusItem() {
        this.minusItem.click();
    }

    public void clickOnDeleteItem() {
        this.deleteItem.click();
    }

    public void clickOnContinueShopping() {
        this.continueShopping.click();
    }

    public void clickOnCheckOut() {
        this.checkout.click();
    }

}
