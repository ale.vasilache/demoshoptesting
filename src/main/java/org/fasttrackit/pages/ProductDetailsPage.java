package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ProductDetailsPage {
    private final SelenideElement title = $("small.text-muted");
    private final SelenideElement photo = $(".product-image");
    private final SelenideElement price = $("div.col.col-lg-2.text-center.col:nth-child(3) > p:nth-child(1)");
    private final SelenideElement basket = $(".fa-cart-plus");
    private final SelenideElement wishlistAdd = $(".fa-heart.fa-3x");
    private final SelenideElement wishlistRemove = $("[data-icon=heart-broken]");
    private final SelenideElement stock = $("p:nth-child(4) > small:nth-child(1)");

    /**
     * Getters
     */
    public SelenideElement getTitleDisplay() {
        return title;
    }

    public String getTitleInfo() {
        return title.text();
    }

    public SelenideElement getPhoto() {
        return photo;
    }

    public SelenideElement getPriceDisplay() {
        return price;
    }

    public String getPriceInfo() {
        return price.text();
    }

    public SelenideElement getBasket() {
        return this.basket;
    }

    public SelenideElement getWishlistAdd() {
        return this.wishlistAdd;
    }

    public SelenideElement getWishlistRemove() {
        return this.wishlistRemove;
    }

    public SelenideElement getStockDisplay() {
        return this.stock;
    }

    public String getStockInfo() {
        return stock.text();
    }

    /**
     * Clicks
     */
    public void AddToBasket() {
        System.out.println("Clicked on the Add to Cart Button !");
        this.basket.click();
        sleep(150);
    }

    public void AddToFavorites() {
        System.out.println("Clicked on the Add to Favorite Button !");
        this.wishlistAdd.click();
        sleep(150);
    }

    public void RemoveFromFavorites() {
        System.out.println("Clicked on the Remove from Favorite Button");
        this.wishlistRemove.click();
        sleep(150);
    }

}
