# Final project for Demo shop application testing

### Contact Person/Course
- Tester: Vasilache Alexandra
- Course: Quality Assurance Group 30

### Short Description
Thi application is responsible for testing the main functionalities of the Demo Shop App.
- Login
- Product Listing
- Add To Wishlist
- Add To Cart
- Checkout

### Tech stack used
- Java 18
- Maven
- Selenide Framework
- Page Object Models

### How to run the tests
`git clone https://gitlab.com/ale.vasilache/demoshoptesting.git`
Execute the following commands to:

### Execute all tests
- `mvn clean tests`

### Generate Report
- `mvn allure:report`
- Open and present report
- `mvn allure:serve`

### Page Objects
- Home page
- Header
- Footer
- Body/ Products
- Product detail page
- Cart page
- Favorite page
- Checkout page
- Order summary page
- Order complete page

### Tests implemented


Reporting is also available in the Allure-results folder.
Tests are executed using maven

#### 1. End-to-End Test 

1. user_dino_end_to_end_experience_adding_product1_to_cart
#### 2. Scenarios

2.1 Cart Page Tests
1. verify_increasing_the_product_quantity_in_cart_page
2. verify_decreasing_the_product_quantity_in_cart_page_to_minimum_1
3. verify_decreasing_the_product_quantity_in_cart_page_to_0
4. verify_trash_button_from_cart_page_when_one_product_on_cart
5. verify_continue_shopping_button_from_cart_page
6. verify_continue_checkout_button_from_cart_page

2.2 Checkout Page Tests
1. user_can_checkout_with_correct_credentials
2. user_cannot_checkout_without_firstname
3. user_cannot_checkout_without_lastname
4. user_cannot_checkout_without_address
5. user_cannot_checkout_without_credential
6. user_can_cancel_checkout

2.3 Login Tests
1. user_can_login_on_the_demo_app_test
2. user_cannot_login_with_locked_account_test
3. user_cannot_login_on_page_without_password_test
4. user_cannot_login_on_page_with_wrong_password_test
5. user_cannot_login_on_page_without_username_test
6. user_cannot_login_on_page_with_wrong_username_test

2.4 Order Summary and Order Completion
1. user_can_finalize_checkout_in_order_summary_page
2. user_can_cancel_checkout_in_order_summary_page
3. user_can_continue_shopping_after_checkout_in_order_complete_page

2.5 Product HomePage Test
1. verify_product_page_can_open_test
2. verify_price_on_product_page_same_as_one_from_home_page
3. verify_default_user_can_add_product_to_cart_test

2.6 Product Page Test
1. user_can_add_a_product_to_cart_test
2. user_can_add_a_product_to_favorite_test
3. user_can_remove_a_product_to_favorite_test

2.7 Wishlist Home Page Test
1. user_can_add_product_to_wishlist_test
2. verify_product_can_be_removed_from_wishlist


#### **3. Static Component Test**

3.1 Cart Page 
1. verify_cart_page_static_component_in_default_mode
2. verify_cart_page_when_one_product_is_added

3.2 Checkout Page
1. verify_static_component_checkout_page_in_default_mode

3.3 Login Modal
1. verify_Login_modal_components_are_displayed_and_modal_can_be_closed_test

3.4 Order Complete Page
1. verify_static_component_in_order_completion_page

3.5 Order Summary Page
1. verify_static_component_checkout_page_product1_in_cart

3.6 Product Detail Page
1. verify_product_page_detail_page_test

3.6 Sort Page
1. verify_sort_field_is_displayed_test
2. verify_default_AtoZ_sort_page_component_test
3. verify_Selected_ZtoA_sort_page_component_test
4. verify_Selected_LowToHigh_sort_page_component_test
5. verify_Selected_HighToLow_sort_page_component_test

3.7 Static Application
1. verify_demo_shop_app_title_test
2. verify_demoShop_footer_build_date_details_test
3. verify_demoShop_footer_contains_question_icon_test
4. verify_demoShop_footer_contains_reset_icon_test
5. verify_demo_shop_header_contains_logo_icon_test
6. verify_demo_shop_header_contains_cart_icon_test
7. verify_demo_shop_header_contains_greetings_message
8. verify_Demo_Shop_Header_Contains_Login_Button
9. verify_demo_shop_header_contains_wishlist_icon_test
